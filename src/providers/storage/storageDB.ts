import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage-angular";
import { Platform } from "@ionic/angular";
import { Usuario } from "src/interfaces/usuario/usuario";

@Injectable()

export class StorageDB {
    private _storage: Storage | null = null
    public Usuario: Usuario[] = null
    constructor(private storage: Storage, public platform: Platform){ this.init()}
    
    async init()
    {
        const storage = await this.storage.create()
        this._storage = storage
    }

    public set(key: string, value:any)
    {
        this._storage?.set(key, value)
    }

    guardar_component_storageDB(key:string, obj:any)
    {
        this.storage.set(key, obj)
    }

    cargar_component_storageDB(key:string)
    {
        let promise = new Promise( ( resolve, reject ) => {
            this.storage.get( key ).then( obj => {
                resolve( obj )
            })
        })
        return promise
    }

    eliminar_component_storageDB(key:string, obj:any)
    {
        this.storage.set( key, obj )
        this.storage.remove( key )
    }
}