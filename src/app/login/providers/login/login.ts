import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class LoginProvider {
    urlLogin = 'https://192.168.1.19/LibertApi/public/api/Admin/Login'
    constructor(public http: HttpClient){}

    postLogin( obj:any ) 
    {
        return new Promise( ( resolve, reject ) => {
            const options = {
                headers: {
                    'Content-Type':'application/json'
                }
            };
            this.http.post(this.urlLogin, obj, options).subscribe( data => {
                resolve( data )
                console.log('login__ ', data)
            }, error => {
                console.log( error )
            })
        })
    }
}