import { Component, OnInit } from '@angular/core';
import { StorageDB } from 'src/providers/storage/storageDB';
import { Router } from '@angular/router';
import { LoginProvider } from './providers/login/login';
import { AlertaService } from '../servicios/alerta.services';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  text_error_usuario = '' 
  text_error_password = ''
  text_error_general = ''
  usuario:string = ''
  password:string = ''
  public usuario_cache: any = []
  login_data = { Persona_idPersona: '', Usuario_idUsuario:''}
  login_temp = { usuario:'', password:''}

  constructor(public storageDB: StorageDB, public router: Router, public loginProv: LoginProvider, public alertaServ: AlertaService) { }

  ngOnInit() {
  }

  login() 
  {
    this.login_temp.usuario = this.usuario
    this.login_temp.password = this.password
    let encode = {usuario: this.usuario, password: this.password}

    this.loginProv.postLogin( encode ).then( ( obj:any ) => {
      let estado = obj.login
      if ( estado == 'ok')
      {
        console.log( estado )
        this.login_data.Persona_idPersona = obj.Persona_idPersona
        this.login_data.Usuario_idUsuario = obj.Usuario_idUsuario
        this.usuario_cache.push( this.login_data )
        this.storageDB.Usuario = this.usuario_cache
        this.storageDB.guardar_component_storageDB('usuario_comb', this.storageDB.Usuario)
        this.router.navigate(['/home'])
      }
      if ( estado == 'error')
      {
        let alerta = {mensaje:'Usuario y/o password incorrecto', duracion:'3000', color:'warning'}
        this.alertaServ.mostrarAlerta( alerta )
      }
    })
  }
  resetFields()
  {
    this.text_error_usuario = '' 
    this.text_error_password = ''
    this.text_error_general = ''
  }
}
