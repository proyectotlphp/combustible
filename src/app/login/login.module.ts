import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { LoginProvider } from './providers/login/login';
import { HttpClientModule } from '@angular/common/http';
import { StorageDB } from 'src/providers/storage/storageDB';
import { Storage } from '@ionic/storage-angular/';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule, 
    HttpClientModule
  ],
  declarations: [LoginPage], 
  providers:[LoginProvider, StorageDB, Storage]
})
export class LoginPageModule {}
