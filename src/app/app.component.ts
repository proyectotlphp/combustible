import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { StorageDB } from 'src/providers/storage/storageDB';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
  home = '/home'
  frm_combustible = '/frm-combustible'
  frm_revisar_abastecimiento = '/frm-revisar-abastecimiento'
  usuario:any


  constructor(
    public menuCttrl: MenuController,
    private platform: Platform,
    public router: Router,
    public storageDB: StorageDB
    ) {
      console.log('se ejecuto aquí..')
      platform.ready().then( () => {
        this.storageDB.cargar_component_storageDB('usuario_comb').then( (obj:any) => {
          this.usuario = obj
          if (this.usuario != null) 
          {
            this.router.navigate(['/home'])
          } else { 
            this.router.navigate(['/login'])
          }
        })

      })
    }

  openPage( pagina:any )
  {
    this.router.navigate([pagina])
    this.menuCttrl.close()
  }
}
