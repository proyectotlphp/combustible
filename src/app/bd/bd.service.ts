import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { AlertaService } from '../servicios/alerta.services';

@Injectable({
  providedIn: 'root'
})
export class BdService {
  private dbInstance: SQLiteObject
  readonly db_name: string = 'combustible.db'
  readonly db_abastecimiento: string = 'tbl_abastecimiento'
  readonly db_placas: string = 'tbl_placas'
  ABASTECIMIENTO: Array <any>
  ABASTECIMIENTO_API: Array <any>
  PLACAS: Array <any>

  constructor(
    private platform: Platform,
    private sqlite: SQLite,  
    public alertaServ: AlertaService
    ) 
    { 
      this.databaseConn()
    }

    databaseConn() {
      this.platform.ready().then( () => {
        this.sqlite.create({
          name: this.db_name, 
          location: 'default'
        }). then( ( sqLite: SQLiteObject ) => {
          this.dbInstance = sqLite 
          //tbl_abastecimiento
          sqLite.executeSql(`
          CREATE TABLE IF NOT EXISTS ${this.db_abastecimiento}(
            id_abastecimiento integer primary key AUTOINCREMENT, 
            Persona_idPersona varchar(10),
            Descripcion_Persona varchar(255),
            Operador_idOperador varchar(10), 
            Descripcion_Operador varchar(255), 
            id_Placa varchar(10), 
            Descripcion_Placa varchar(150),
            fecha date, 
            rpc_estado varchar(20)
          )`, [])
          .then( ( res ) => {})
          .catch( ( error ) => alert(JSON.stringify( error )))

          //tbl_placas
          sqLite.executeSql(`
          CREATE TABLE IF NOT EXISTS ${this.db_placas}(
            idVehiculo integer primary key AUTOINCREMENT,
            placaVeh varchar(150)
          )`, [])
          .then( ( res ) => {})
          .catch( ( error ) => alert(JSON.stringify( error )))
        })
      })
    }

    //Insertar Abastecimiento
    public addAbastecimiento( Persona_idPersona, Descripcion_Persona, Operador_idOperador, Descripcion_Operador, id_Placa, Descripcion_Placa){
      if (!Persona_idPersona.length || !Descripcion_Persona.length || !Operador_idOperador.length || !Descripcion_Operador.length || !Descripcion_Placa.length) {
        //this.mostrarAlerta('Los campos no pueden estar vacios')
        let alerta = { mensaje:'Los campos no pueden estar vacios', duracion:'3000', color:'danger'}
        this.alertaServ.mostrarAlerta( alerta )
        return
      }
      this.dbInstance.executeSql(`
      INSERT INTO ${this.db_abastecimiento} (Persona_idPersona, Descripcion_Persona, Operador_idOperador, Descripcion_Operador, id_Placa, Descripcion_Placa, fecha, rpc_estado)
                            VALUES          ('${Persona_idPersona}','${Descripcion_Persona}','${Operador_idOperador}','${Descripcion_Operador}','${id_Placa}','${Descripcion_Placa}', Date('now'), 'insert')`, [])
      .then( () => { console.log( id_Placa + ' _Success'); this.getAbastecimiento()}, ( e ) => { alert(JSON.stringify( e.err ))} )
    }

    //Insertar placas
    public addPlacas( idVehiculo, placaVeh) {
      if (idVehiculo.length < 0 || placaVeh.length < 0) {
        let alerta = { mensaje:'Los campos no pueden estar vacios', duracion:'3000', color:'danger'}
        this.alertaServ.mostrarAlerta( alerta )
        return
      }
      this.dbInstance.executeSql(`
      INSERT INTO ${this.db_placas} (idVehiculo, placaVeh)
                            VALUES  ('${idVehiculo}','${placaVeh}')`, [])
      .then( () => { console.log(idVehiculo + ' _Success'); this.getPlacas() }, ( e ) => { console.log( e )} )
    }

    //Recupera Informacion
    getAbastecimiento() {
      return this.dbInstance.executeSql(`SELECT * FROM ${this.db_abastecimiento} where rpc_estado = 'insert'`, []).then( ( res ) => {
        this.ABASTECIMIENTO = []
        if ( res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++)
          {
            this.ABASTECIMIENTO.push( res.rows.item(i) )
          }
          return this.ABASTECIMIENTO
        }
      }, ( e ) => { alert(JSON.stringify( e ))})
    }
    
    getAbastecimientoAPI(Persona_idPersona) 
    {
      return this.dbInstance.executeSql(`SELECT Operador_idOperador, Descripcion_Operador, id_Placa from ${this.db_abastecimiento} WHERE Persona_idPersona = ?`, [Persona_idPersona])
      .then( ( res ) => {
        this.ABASTECIMIENTO_API = []
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++)
          {
            this.ABASTECIMIENTO_API.push(res.rows.item(i))
          }
          return this.ABASTECIMIENTO_API
        }
      }, ( e ) => {
        alert(JSON.stringify( e ))
      })
    }

    //Actualizar Abastecimiento API 
    ActualizarAbastecimientoAPI( id_abastecimiento, rpc_estado )
    {
      let data = [rpc_estado]
      return this.dbInstance.executeSql(`UPDATE ${this.db_abastecimiento} SET rpc_estado = ? WHERE id_abastecimiento = ${id_abastecimiento}`, data)
      .then( ( res ) => {
        this.getAbastecimiento()
      }, ( e ) => {
        alert( JSON.stringify( e ))
      })
    }

    //Recuperar Placas 
    getPlacas() {
      return this.dbInstance.executeSql(`SELECT * FROM ${this.db_placas}`, []).then( ( res ) => {
        this.PLACAS = []
        return res
        /*if ( res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++)
          {
            this.PLACAS.push( res.rows.item( i ))
          }
          return this.PLACAS
        }*/
      }, ( e ) => { alert(JSON.stringify( e ))})
    }

    //Eliminar Registro
    eliminarAbastecimiento(id_abastecimiento) {
      this.dbInstance.executeSql(`
      DELETE FROM ${this.db_abastecimiento} WHERE id_abastecimiento = ${id_abastecimiento}`, [])
      .then( () => {
        console.log(id_abastecimiento, ' __eliminado')
        this.getAbastecimiento()
      }).catch( e => { alert(JSON.stringify( e ))})
    }
}
