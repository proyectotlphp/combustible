import { Component, OnInit } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Operador } from './interfaces/operador';
import { OperadorService } from './servicios/operador.services';
import { OperadorProvider } from './providers/operador/operador';
import { Placa } from './interfaces/placa';
import { PlacaService } from './servicios/placa.services';
import { PlacaProvider } from './providers/placa/placa';
import { BdService } from 'src/app/bd/bd.service';
import { AlertaService } from 'src/app/servicios/alerta.services';
import { EnviarAbastecimientoProvider } from './providers/enviar_abastecimiento/enviar_abastecimiento';
import { StorageDB } from 'src/providers/storage/storageDB';

@Component({
  selector: 'app-frm-combustible',
  templateUrl: './frm-combustible.page.html',
  styleUrls: ['./frm-combustible.page.scss'],
})
export class FrmCombustiblePage implements OnInit {

  public operador: Operador[] = null
  public operado: Operador
  public placa: Placa[] = null
  public plac: Placa
  public date_picker_start = ''

  abastecimiento  = { operador: '', desc_operador:'', placa: '', desc_placa:'', abastecimiento:''}
  abastecimiento_itm:any
  items:any
  cont_ = 0
  datos_usuario: any
  itemsABS:any
  enviar_api = {
    Persona_idPersona: 0, 
    fechaRep: '', 
    abs: {
      Descripcion_Operador: '', 
      Operador_idOperador: '', 
      id_Placa: ''
    }
  }

  constructor(
    public operadorProv: OperadorProvider, 
    public operadorService: OperadorService, 
    public placaProv: PlacaProvider, 
    public placaService: PlacaService, 
    public crud: BdService, 
    public alertService: AlertaService, 
    public abastecimientoProv: EnviarAbastecimientoProvider, 
    public storageDB: StorageDB
  ) { 

    this.crud.databaseConn()
    this.operador = this.operadorService.getOperador()
    this.placa = this.placaService.getPlaca()

    this.storageDB.cargar_component_storageDB('usuario_comb').then( ( obj: any ) => 
    {
      this.storageDB.Usuario = obj
      this.datos_usuario = this.storageDB.Usuario
    })

    let fech = new Date()
    let stringYear = fech.getFullYear()
    let stringMonth = fech.getMonth() + 1
    let stringDay = `${(fech.getDate())}`.padStart(2,'0')

    this.date_picker_start = `${stringYear}-${stringMonth}-${stringDay}`
    console.log( this.date_picker_start )
  }

  operadorChange( event: {
    component: IonicSelectableComponent, 
    value: any
  }){
    this.abastecimiento.operador = event.value.idVehiculo.toString()
    this.abastecimiento.desc_operador = event.value.placaVeh.toString()
  }

  placaChange( event: {
    component: IonicSelectableComponent, 
    value: any
  }){
    this.abastecimiento.placa = event.value.idVehiculo.toString()
    this.abastecimiento.desc_placa = event.value.placaVeh.toString()
  }

  ionViewDidEnter()
  {
    this.crud.getAbastecimiento()
    console.log('SQLITE::', this.crud.ABASTECIMIENTO)
  }

  ngOnInit() {
  }

  agregarAbs()
  {
    if (this.abastecimiento.placa != '' && this.abastecimiento.operador !='') 
    {
      //this.arrayAbastecimiento['placa'].push(this.abastecimiento.placa)
      //this.arrayAbastecimiento['operador'].push(this.abastecimiento.operador)
      //this.arrayAbastecimiento.push(this.abastecimiento)
      //this.array.push(this.arrayAbastecimiento)
      //(Persona_idPersona: any, Descripcion_Persona: any, Operador_idOperador: any, Descripcion_Operador: any, id_Placa: any, Descripcion_Placa: any)
      console.log('usuario_comb:: ', this.datos_usuario)
      this.crud.addAbastecimiento(this.datos_usuario[0].Persona_idPersona,'Usuario', this.abastecimiento.operador, this.abastecimiento.desc_operador, this.abastecimiento.placa, this.abastecimiento.desc_placa)
      this.operado = null
      this.plac = null
      this.abastecimiento.placa = ''
      this.abastecimiento.operador = ''
      this.cont_ = this.cont_ + 1
      //this.crud.getAbastecimiento()
      //this.abastecimiento_itm = this.crud.ABASTECIMIENTO
      //console.log('agregado:: ', this.crud.ABASTECIMIENTO)
      //this.items = this.crud.ABASTECIMIENTO
      //console.log('cache:: ', this.items)
      //this.mostrarAbastecimiento()
      console.log('contador:: ', this.cont_)
    }
  }
  eliminarItem(id:number) 
  {
    this.crud.eliminarAbastecimiento(id)
    let alerta = {mensaje:'Item eliminado ...', duracion:'3000', color:'success'}
    this.alertService.mostrarAlerta(alerta)
    this.cont_ = this.cont_ -1
    console.log('cont:: ', this.cont_)
  }

  solicitarAbs( datos: any ) 
  {
    this.abastecimientoAPI(this.datos_usuario[0].Persona_idPersona)

    this.items = datos 
    for (var i = 0; i < this.items.length; i++)
    {
      //console.log('salida:: ', this.items[i])
    } 
    let json_cabecera = JSON.stringify( this.items )
    let encode = { abs: json_cabecera }
    //console.log('json_cabezera:: ', encode)
  }

  async abastecimientoAPI(Persona_idPersona)
  {
    let promise = new Promise( ( resolve, reject ) => {
      this.crud.getAbastecimientoAPI( Persona_idPersona ).then( ( res:any ) => {
        resolve( res )
      }, error => {
        console.log( error )
      })
    })
    this.itemsABS = await promise
    //console.log('itemsABS:: ', this.itemsABS)
    console.log('itemsABS:: ', this.itemsABS)
    this.enviar_api.Persona_idPersona = this.datos_usuario[0].Persona_idPersona
    this.enviar_api.fechaRep = this.date_picker_start 
    this.enviar_api.abs = this.itemsABS
    console.log('enviar_api:: ', this.enviar_api)
    
    for (var i = 0; i < this.crud.ABASTECIMIENTO.length; i++) 
    {
      console.log('id_abastecimiento:: ', this.crud.ABASTECIMIENTO[i].id_abastecimiento)
      this.crud.ActualizarAbastecimientoAPI(this.crud.ABASTECIMIENTO[i].id_abastecimiento, 'OK')
    }
    console.log('abastecimiento:: ', this.crud.ABASTECIMIENTO)
  }
}
