import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrmCombustiblePage } from './frm-combustible.page';

const routes: Routes = [
  {
    path: '',
    component: FrmCombustiblePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrmCombustiblePageRoutingModule {}
