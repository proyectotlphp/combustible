import { Injectable } from "@angular/core";
import { Operador } from "../interfaces/operador";
import { OperadorProvider } from "../providers/operador/operador";

@Injectable() 
export class OperadorService { 
    public operador_temp: Operador[] = null
    constructor(public operadorProv: OperadorProvider){}

    getOperador(page?: number, size?: number):Operador[] {
        let operador = [] 
        this.operadorProv.getOperadorHTTP().then( ( data:any ) => {
            this.operador_temp = data 
            this.operador_temp.forEach( e => {
                operador.push( e )
            })
        })
        if (page && size) 
        {
            operador = operador.slice((page -1) * size, ((page -1) * size) + size)
        }
        return operador
    }
}