import { Injectable } from "@angular/core";
import { Placa } from "../interfaces/placa";
import { PlacaProvider } from "../providers/placa/placa";

@Injectable()
export class PlacaService {
    public placa_temp: Placa[] = null
    array_pesada = []
    array_acople = [] 
    array_liviana = []
    items:any
    array_placa = []

    constructor(
        public placaProv: PlacaProvider
        )
    {

        //Almacenar placas en SQLite
        
        /*let placa = []
        this.placaProv.getPlacaPesadaHTTP().then( ( data:any ) => {
            this.array_pesada = data
            //console.log('array_pesada:: ', this.array_pesada)
            /*for( var i = 0; i < placa.length; i++)
            {
                this.crud.addPlacas(placa[i].idVehiculo, placa[i].placaVeh)
            }
        })*/
       

    }

    async recuperarPlacaPesada()
    {
        let promise = new Promise( ( resolve, reject) => {
            this.placaProv.getPlacaPesadaHTTP().then( ( res:any ) => {
                resolve( res )
            }, error => {
                console.log( error )
            })
        })
        let pesada: any 
        pesada = await promise
        for (var i = 0; i < pesada.length; i++)
        {
           this.array_pesada.push(pesada[i])
        }
    }

    async recuperarPlacaAcople() 
    {
        let promise = new Promise( ( resolve, reject ) => {
            this.placaProv.getPlacaAcopleHTTP().then( ( res:any ) => {
                resolve( res )
            }, error => {
                console.log( error )
            })
        })
        let acople: any
        acople = await promise
        for (var i =0; i < acople.length; i++)
       
        {
            this.array_acople.push(acople[i])
        }
    }

    async recuperarPlacaLiviana() 
    {
        let promise = new Promise( ( resolve, reject ) => {
            this.placaProv.getPlacaLivianaHTTP().then( ( res:any ) => {
                resolve( res )
            }, error => {
                console.log( error )
            })
        })
        let liviana:any 
        liviana = await promise
        for (var i = 0; i < liviana.length; i++)
        {
            this.array_liviana.push( liviana[i] )
        }
        this.array_placa = this.array_pesada.concat( this.array_acople, this.array_liviana )
        //console.log('liviana:: ', liviana)
        //console.log('array_placa :: ', this.array_placa )
    }

    /*async recuperarPlacaSQLite() 
    {
        let promise = new Promise( ( resolve, reject ) => {
            this.placaProv.getPlacasSQLite().then( ( data:any ) => {
                resolve( data )
            }, error => {
                console.log( error )
            })
        })
        let data = await promise
        console.log('data::', data )
    }*/

    getPlaca(page?: number, size?: number):Placa[] {
        let placa = []
        let pesado:any
        let acople:any 
        let liviano:any 

        this.placaProv.getPlacaPesadaHTTP().then( ( data:any ) => {
            pesado = data
            pesado.forEach(element => {
                this.array_pesada.push( element )
            });
            console.log('pesado:: ', this.array_pesada)
            
            this.placaProv.getPlacaAcopleHTTP().then( ( data:any ) => {
                acople = data
                acople.forEach(element => {
                    this.array_acople.push( element )
                });
                console.log('acople:: ', this.array_acople)

                this.placaProv.getPlacaLivianaHTTP().then( ( data:any ) => {
                    liviano = data 
                    liviano.forEach(element => {
                        this.array_liviana.push( element )
                    });
                    console.log('liviano:: ', this.array_liviana)
                    this.array_placa = this.array_pesada.concat( this.array_acople, this.array_liviana )
                    console.log('array_placa1:: ', this.array_placa)

                    this.array_placa.forEach(e => {
                        placa.push( e )
                    })

                    if (page && size)
                    {
                        placa = placa.slice((page -1) * size, ((page -1) * size) + size)
                    }
                })
            })
        })
        this.array_pesada = []
        this.array_acople = []
        this.array_liviana = []
        this.array_placa = []
        return placa

        /*console.log('array_placa2:: ', this.array_placa)
        this.placaProv.getPlacaAcopleHTTP().then( ( data:any ) => {
            this.placa_temp = data
            this.placa_temp.forEach( e => {
                placa.push( e )
            })
        })
        if (page && size)
        {
            placa = placa.slice((page -1) * size, ((page -1) * size) + size)
        }
        return placa*/
    }
}