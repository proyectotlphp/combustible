import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';

@Injectable()
export class OperadorProvider {
    url = 'https://192.168.1.19/LibertApi/public/api/Mantenimiento/Vehiculo/PESADA'

    constructor(public http: HttpClient){}

    getOperadorHTTP()
    {
        return new Promise( resolve => { 
            this.http.get( this.url ).subscribe( data=> {
                resolve( data )
            }, error => {
                console.log( error )
            })
        })
    }
}