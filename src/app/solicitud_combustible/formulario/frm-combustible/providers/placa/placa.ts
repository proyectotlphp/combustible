import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class PlacaProvider {
    url_pesada = 'https://192.168.1.19/LibertApi/public/api/Mantenimiento/Vehiculo/PESADA'
    url_acople = 'https://192.168.1.19/LibertApi/public/api/Mantenimiento/Vehiculo/ACOPLE'
    url_liviana = 'https://192.168.1.19/LibertApi/public/api/Mantenimiento/Vehiculo/LIVIANA'

    constructor(public http: HttpClient)
    { 
    }

    getPlacaAcopleHTTP()
    {
        return new Promise( resolve => { 
            this.http.get( this.url_acople ).subscribe( data => {
                resolve( data )
            }, error => {
                console.log( error )
            })
        })
    }
    getPlacaPesadaHTTP()
    {
        return new Promise( resolve => {
            this.http.get( this.url_pesada ).subscribe( data => { 
                resolve( data )
            }, error => {
                console.log( error )
            })
        })
    }
    getPlacaLivianaHTTP()
    {
        return new Promise( resolve => {
            this.http.get( this.url_liviana ).subscribe( data => { 
                resolve( data )
            }, error => { 
                console.log( error )
            })
        })
    }
}