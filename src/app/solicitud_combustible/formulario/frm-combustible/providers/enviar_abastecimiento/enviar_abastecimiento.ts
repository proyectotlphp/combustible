import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class EnviarAbastecimientoProvider {
    url_enviar = ''
    constructor(public http: HttpClient){}

    postEnviarAbastecimiento( obj:any ) 
    {
        return new Promise( ( resolve, reject ) => {
            const options = {
                headers: {
                    'Content-Type':'application/json',
                }
            }; 
            this.http.post(this.url_enviar, obj, options).subscribe( data => {
                resolve( data )
                console.log('reporte_abastecimiento__ ', data)
            }, error => {
                console.log( error )
            })
        })
    }
}