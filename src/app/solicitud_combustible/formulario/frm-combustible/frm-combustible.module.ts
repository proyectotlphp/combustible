import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrmCombustiblePageRoutingModule } from './frm-combustible-routing.module';

import { FrmCombustiblePage } from './frm-combustible.page';

import { IonicSelectableModule } from 'ionic-selectable';
import { OperadorService } from './servicios/operador.services';
import { OperadorProvider } from './providers/operador/operador';
import { PlacaService } from './servicios/placa.services';
import { PlacaProvider } from './providers/placa/placa';
import { HttpClientModule } from '@angular/common/http';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { BdService } from 'src/app/bd/bd.service';
import { IonicStorageModule } from '@ionic/storage-angular';
import { EnviarAbastecimientoProvider } from './providers/enviar_abastecimiento/enviar_abastecimiento';

@NgModule({
  schemas:[ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicSelectableModule,
    HttpClientModule,
    FrmCombustiblePageRoutingModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb','sqlite','websql']
    }) 
  ],
  declarations: [FrmCombustiblePage], 
  providers:[ OperadorService, OperadorProvider, PlacaService, PlacaProvider, SQLite, BdService, EnviarAbastecimientoProvider]
})
export class FrmCombustiblePageModule {}
