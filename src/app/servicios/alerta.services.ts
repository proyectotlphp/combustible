import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})

export class AlertaService {
    constructor(public toastCtrl: ToastController){}

    async mostrarAlerta( alerta:any ) {
        const toast = await this.toastCtrl.create({
            message: alerta.mensaje, 
            duration: alerta.duracion, 
            color: alerta.color
        })
        toast.present()
    }
}