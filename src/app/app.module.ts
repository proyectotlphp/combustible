import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { StorageDB } from 'src/providers/storage/storageDB';
import { Storage } from '@ionic/storage'
import { IonicStorageModule } from '@ionic/storage-angular';

@NgModule({
  schemas:[ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, IonicStorageModule.forRoot({
    name: '__mydb', 
    driverOrder: ['indexeddb','sqlite','websql']
  })],
  providers: [Storage, StorageDB, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
