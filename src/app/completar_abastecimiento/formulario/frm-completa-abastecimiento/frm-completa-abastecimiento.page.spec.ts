import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FrmCompletaAbastecimientoPage } from './frm-completa-abastecimiento.page';

describe('FrmCompletaAbastecimientoPage', () => {
  let component: FrmCompletaAbastecimientoPage;
  let fixture: ComponentFixture<FrmCompletaAbastecimientoPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FrmCompletaAbastecimientoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FrmCompletaAbastecimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
