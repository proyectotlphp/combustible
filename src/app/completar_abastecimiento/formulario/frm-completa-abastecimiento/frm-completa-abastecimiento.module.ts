import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrmCompletaAbastecimientoPageRoutingModule } from './frm-completa-abastecimiento-routing.module';

import { FrmCompletaAbastecimientoPage } from './frm-completa-abastecimiento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FrmCompletaAbastecimientoPageRoutingModule
  ],
  declarations: [FrmCompletaAbastecimientoPage]
})
export class FrmCompletaAbastecimientoPageModule {}
