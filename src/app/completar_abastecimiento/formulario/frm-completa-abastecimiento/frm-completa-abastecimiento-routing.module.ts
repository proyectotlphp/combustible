import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrmCompletaAbastecimientoPage } from './frm-completa-abastecimiento.page';

const routes: Routes = [
  {
    path: '',
    component: FrmCompletaAbastecimientoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrmCompletaAbastecimientoPageRoutingModule {}
