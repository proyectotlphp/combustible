import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'frm-combustible',
    loadChildren: () => import('./solicitud_combustible/formulario/frm-combustible/frm-combustible.module').then( m => m.FrmCombustiblePageModule)
  },
  {
    path: 'frm-completa-abastecimiento',
    loadChildren: () => import('./completar_abastecimiento/formulario/frm-completa-abastecimiento/frm-completa-abastecimiento.module').then( m => m.FrmCompletaAbastecimientoPageModule)
  },
  {
    path: 'frm-revisar-abastecimiento',
    loadChildren: () => import('./revisar_abastecimiento/formulario/frm-revisar-abastecimiento/frm-revisar-abastecimiento.module').then( m => m.FrmRevisarAbastecimientoPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
