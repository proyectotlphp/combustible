import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FrmRevisarAbastecimientoPage } from './frm-revisar-abastecimiento.page';

describe('FrmRevisarAbastecimientoPage', () => {
  let component: FrmRevisarAbastecimientoPage;
  let fixture: ComponentFixture<FrmRevisarAbastecimientoPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FrmRevisarAbastecimientoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FrmRevisarAbastecimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
