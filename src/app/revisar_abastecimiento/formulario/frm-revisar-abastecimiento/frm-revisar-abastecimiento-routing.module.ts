import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrmRevisarAbastecimientoPage } from './frm-revisar-abastecimiento.page';

const routes: Routes = [
  {
    path: '',
    component: FrmRevisarAbastecimientoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrmRevisarAbastecimientoPageRoutingModule {}
