import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrmRevisarAbastecimientoPageRoutingModule } from './frm-revisar-abastecimiento-routing.module';

import { FrmRevisarAbastecimientoPage } from './frm-revisar-abastecimiento.page';
import { Camera } from '@ionic-native/camera/ngx';
import { HttpClientModule } from '@angular/common/http';
import { RevisarAbastecimientoProvider } from './providers/enviar_revisar_abastecimiento/revisar_abastecimiento';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    FrmRevisarAbastecimientoPageRoutingModule
  ],
  declarations: [FrmRevisarAbastecimientoPage],
  providers:[ Camera, RevisarAbastecimientoProvider ]
})
export class FrmRevisarAbastecimientoPageModule {}
