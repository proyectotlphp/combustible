import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';
import { AlertaService } from 'src/app/servicios/alerta.services';
import { StorageDB } from 'src/providers/storage/storageDB';
import { RevisarAbastecimientoProvider } from './providers/enviar_revisar_abastecimiento/revisar_abastecimiento';


@Component({
  selector: 'app-frm-revisar-abastecimiento',
  templateUrl: './frm-revisar-abastecimiento.page.html',
  styleUrls: ['./frm-revisar-abastecimiento.page.scss'],
})
export class FrmRevisarAbastecimientoPage implements OnInit {
  
  public date_picker_start = ''
  clickedImage: any
  kbytes: number
  datos_usuario: any
  options: CameraOptions = 
  {
    quality: 80, // calidad de la imagen de 0 a 100 
    destinationType: this.camera.DestinationType.DATA_URL, 
    targetWidth: 1500, 
    targetHeight: 1500
    //encodingType: this.camera.EncodingType.JPEG, 
    //mediaType: this.camera.MediaType.PICTURE
  }

  abastecimiento = [
    {
      img: '', 
      size:'',
      fecha: '23/12/2021', 
      operador: 'Roger Lider Alagon Santos', 
      placa: 'DRD-456', 
      galonaje: '', 
      kilometraje: ''
    }, 
    {
      img: '',
      size:'', 
      fecha: '24/12/2021', 
      operador: 'Operador de Ejemplo', 
      placa: 'LRE-123', 
      galonaje: '', 
      kilometraje: ''
    },/*
    {
      img: '', 
      fecha: '25/12/2021', 
      operador: 'Operador Mantenimiento', 
      placa: 'JKL-789', 
      galonaje: '', 
      kilometraje: ''
    }, 
    {
      img: '', 
      fecha: '26/12/2021', 
      operador: 'Operador SOMA', 
      placa: 'MJK-112', 
      galonaje: '', 
      kilometraje: ''
    },
    {
      img: '', 
      fecha: '27/12/2021', 
      operador: 'Operador Recursos H.', 
      placa: 'OLP-345', 
      galonaje: '', 
      kilometraje: ''
    }*/
  ]

  enviar_api = {
      fecha: '',
      Persona_idPersona: '', 
      abs : [
        {
          img: '', 
          operador: '', 
          kilometraje: '',
          galonaje: '',
          placa: ''
        }, 
        {
          img: '',
          operador: '',
          kilometraje: '',
          galonaje: '',
          placa: ''
        },
        {
          img: '',
          operador: '',
          kilometraje: '',
          galonaje: '',
          placa: ''
        },
        {
          img: '',
          operador: '',
          kilometraje: '',
          galonaje: '',
          placa: ''
        },
        {
          img: '',
          operador: '',
          kilometraje: '',
          galonaje: '',
          placa: ''
        }
      ], 
      
    }

  constructor(
    private camera: Camera, 
    public alertCtrl: AlertController, 
    public alertaServ: AlertaService, 
    public abastecimientoProv: RevisarAbastecimientoProvider, 
    public storageDB: StorageDB
    ) 
  {

    this.storageDB.cargar_component_storageDB('usuario_comb').then( ( obj: any ) => 
    {
      this.storageDB.Usuario = obj
      this.datos_usuario = this.storageDB.Usuario
    })

    let fech = new Date()
    let stringYear = fech.getFullYear()
    let stringMonth = fech.getMonth() + 1 
    let stringDay = `${(fech.getDate())}`.padStart(2,'0')

    this.date_picker_start = `${stringYear}-${stringMonth}-${stringDay}`
    console.log( this.date_picker_start )
  }

  tomarFoto(i:number)
  {
    this.camera.getPicture(this.options).then( ( imageData ) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData
      console.log( base64Image )
      this.clickedImage = base64Image
      this.abastecimiento[i].img = base64Image
      const new_size = this.calc_image_size( imageData )
      console.log('size=>', new_size, 'KB')
      this.abastecimiento[i].size = new_size + ' KB'
    }, ( error ) => {
      console.log( error )     
    })
  }

  calc_image_size( base64 )
  {
    let y = 1 
    if ( base64.endsWith('==')) {
      y = 2
    } else if ( base64.endsWith('=')) {
      y = 1
    }
    const x_size = ( base64.length * ( 3 / 4 ))
    return Math.round(x_size / 1024)
  }

  seleccionarFoto(i:number) 
  {
    let options = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY, 
      innerWidth: 1500, 
      innerHeight: 1500
    }

    this.camera.getPicture( options ).then( ( imagenData ) => {
      let base64Image = 'data:image/jpeg;base64,' + imagenData
      const size = imagenData.length 
      const kilobytes = size / 1024 
      const megabytes = kilobytes / 1024

      if ( Math.round(kilobytes) > 535)
      {
        this.abastecimiento[i].img = ''
        let alerta = {mensaje:'Tamaño de la imagen no soportado', duracion:'3000', color:'danger'}
        this.alertaServ.mostrarAlerta( alerta )
      }
      
      if ( Math.round(kilobytes) <= 535)
      {
        console.log( base64Image )
        this.clickedImage = base64Image
        this.abastecimiento[i].img = base64Image
        const new_size = this.calc_image_size( imagenData )
        console.log('size=>', new_size, 'KB')
        this.abastecimiento[i].size = new_size + ' KB'
        
        console.log('imagenData:: ', imagenData.length)
        //console.log('kilobytes:: ', kilobytes)
        console.log('kilobytes:: ', Math.round(kilobytes))
        //console.log('megabytes:: ', megabytes)
      }
    }, ( error ) => {
      console.log( JSON.stringify( error ))
    })
  }

  async mostrarImg() 
  {
    const alert = await this.alertCtrl.create({
      message:`<img src="${ this.clickedImage }" width="100%" height="100%"/>`,
      buttons:['Cerrar']
    })
    await alert.present()
    console.log( this.clickedImage)
  }

  resizeBase64Img(base64, newWidth, newHeight) {
    return new Promise((resolve, reject)=>{
        var canvas = document.createElement("canvas");
        canvas.style.width = newWidth.toString()+"px";
        canvas.style.height = newHeight.toString()+"px";
        let context = canvas.getContext("2d");
        let img = document.createElement("img");
        img.src = base64;
        img.onload = function () {
            context.scale(newWidth/img.width,  newHeight/img.height);
            context.drawImage(img, 0, 0); 
            resolve(canvas.toDataURL());               
        }
    });
}

  enviar()
  {
    let cont_reg = 0 
    for (let i in this.abastecimiento)
    {
      if (this.abastecimiento[i].galonaje == '')
      {
        cont_reg = cont_reg + 1
      }
      if (this.abastecimiento[i].kilometraje == '')
      {
        cont_reg = cont_reg + 1
      }
    }

    if (cont_reg > 0) 
    {
      let alerta = { mensaje:'Los campos no pueden estar vacios', duracion:'3000', color:'warning'}
      this.alertaServ.mostrarAlerta( alerta )
    }

    
    console.log('cont_reg:: ', cont_reg)
    if (cont_reg == 0)
    {
      //console.log('api:: ', this.abastecimiento)
      this.enviar_api.Persona_idPersona = this.datos_usuario[0].Persona_idPersona
      this.enviar_api.fecha = this.date_picker_start
      for (let i = 0; i < this.abastecimiento.length; i++)
      {
        this.enviar_api.abs[i].img = this.abastecimiento[i].img
        this.enviar_api.abs[i].kilometraje = this.abastecimiento[i].kilometraje
        this.enviar_api.abs[i].galonaje = this.abastecimiento[i].galonaje
        this.enviar_api.abs[i].operador = this.abastecimiento[i].operador
        this.enviar_api.abs[i].placa = this.abastecimiento[i].placa
      }
      console.log('enviar_api:: ', this.enviar_api)

      let json_cabecera = JSON.stringify(this.enviar_api)
      let encode = { datos: json_cabecera }
      console.log('json_cabecera:: ', encode)
    }
  }

  ngOnInit() {
  }

}
